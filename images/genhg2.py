import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import sys

f=np.load("graphs2.npz")
g=f["q"]
r=f["r"]

print("g",g.shape)
print("r",r.shape)


i=18+1
i=12

if len(sys.argv)>1:
  i=int(sys.argv[1])


g=g[i]
r=r[i]


print(g)
print(r)


siz=int(np.sum(r[:,-1],axis=-1))
phi=r[:siz,4]
eta=r[:siz,5]
E=r[:siz,0]

print("siz",siz)
print("phi",phi)
print("eta",eta)
print("E",E)

phi0=np.min(phi)
phi1=np.max(phi)
eta0=np.min(eta)
eta1=np.max(eta)
dphi=phi1-phi0
deta=eta1-eta0
phi0-=dphi*0.1
phi1+=dphi*0.1
eta0-=deta*0.1
eta1+=deta*0.1

bins=32



# Big bins
# plt.hist2d(phi, eta, bins=(50, 50), weights=E, cmap=plt.cm.jet)
plt.hist2d(phi, eta,range=[[phi0,phi1],[eta0,eta1]], bins=(bins, bins), weights=E, cmap=plt.cm.Blues, norm=mpl.colors.LogNorm())

plt.xlabel("phi")
plt.ylabel("eta")









cbar=plt.colorbar()
cbar.set_label("E", rotation=270)

for i in range(len(g)):
  # for j in range(i,len(g[i])):
  for j in range(0,len(g[i])):
    if g[i][j]>0.0 or False:
      plt.plot([phi[i],phi[j]],[eta[i],eta[j]],color="black",alpha=0.2)


# plt.plot([phi[0],phi[1]],[eta[0],eta[1]],color="black",alpha=0.2)



plt.savefig("ghist.png",format="png")
plt.savefig("ghist.pdf",format="pdf")
plt.show()




exit()





plt.close()

import networkx as nx
import matplotlib.pyplot as plt


seed=12

import random
random.seed(246)        # or any integer
np.random.seed(4812)


G=nx.from_numpy_array(g)


# G=nx.Graph()
# nodes=["1","2","3","4","5"]#,"f","g"]

# G.add_nodes_from(nodes)

# for i in range(1,len(nodes)-1):
  # G.add_edge(nodes[0],nodes[i])

# for i in range(1,len(nodes)-1):
  # G.add_edge(nodes[i],nodes[i+1])
# G.add_edge(nodes[-1],nodes[1])


nx.draw_networkx(G,with_labels=False,node_color="black",font_color="white",font_size=14)
plt.savefig("graph.png",format="png")
plt.savefig("graph.pdf",format="pdf")



plt.show() # display









