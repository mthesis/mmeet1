import numpy as np
import sys
import matplotlib.pyplot as plt





greys=['16a', '20a', '23a','33a','34a','38a','35a'] 
ref="ref/pa"
good="48a"
mage="27a"

nam="zoom"

base="../../ttgan/q/"
plt.close()

def plot(job,color,name,alpha=1.0):
  da="a" in job
  db="b" in job
  job=job.replace("a","").replace("b","")




  if da:
    fa=np.load(base + job +"/outa.npz")
  
    fpr=fa["fpr"]
    tpr=fa["tpr"]
    
    auc=float(fa["auc"])
    acc=float(fa["acc"])

    print(job+"a reached","acc=",acc,"auc=",auc)

    if name==None:
      plt.plot(fpr,tpr,color=color,alpha=alpha)
    else:
      plt.plot(fpr,tpr,label=name+", acc="+str(round(acc,4))+" auc="+str(round(auc,4)),color=color,alpha=alpha)
  if db:
    fa=np.load(base+job +"/outb.npz")
  
    fpr=fa["fpr"]
    tpr=fa["tpr"]
    
    auc=float(fa["auc"])
    acc=float(fa["acc"])

    print(job+"b reached","acc=",acc,"auc=",auc)

    if name==None:
      plt.plot(fpr,tpr,color=color,alpha=alpha)
    else:
      plt.plot(fpr,tpr,label=name+", acc="+str(round(acc,4))+" auc="+str(round(auc,4)),color=color,alpha=alpha)

for g in greys:
  plot(g,"grey",None,alpha=0.5)
plot(greys[0],"grey","usual",alpha=0.5)
plot(ref,"black","1902.08570")
plot(good,"blue","fixed")
plot(mage,"green","strange",alpha=0.8)

plt.legend()
plt.xlabel("False Positive")
plt.ylabel("True Positive")

plt.xlim([-0.03,0.4])
plt.ylim([0.60,1.05])

plt.savefig(nam+".png",format="png")
plt.savefig(nam+".pdf",format="pdf")

print("showing...:")

plt.show()

print("showed")












