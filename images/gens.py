import networkx as nx
import matplotlib.pyplot as plt


seed=12

import random
random.seed(246)        # or any integer
import numpy
numpy.random.seed(4812)



G=nx.Graph()
nodes=["1","2","3","4","5"]#,"f","g"]

G.add_nodes_from(nodes)

for i in range(1,len(nodes)-1):
  G.add_edge(nodes[0],nodes[i])

for i in range(1,len(nodes)-1):
  G.add_edge(nodes[i],nodes[i+1])
G.add_edge(nodes[-1],nodes[1])


nx.draw_networkx(G,with_labels=True,node_color="black",font_color="white",font_size=14)
plt.savefig("simple.png",format="png")
plt.savefig("simple.pdf",format="pdf")



plt.show() # display